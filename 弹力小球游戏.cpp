#include<stdio.h>

#include<math.h>

#include<graphics.h>

#include<conio.h>

#define width 640

#define height 480

#define PI 3.14

float ball_x, ball_y;

int ball_vx, ball_vy;
int radius=20;
int bar_x = width / 2;
//挡板参数
int bar_left, bar_right, bar_top, bar_bottom;

void initData();//数据赋值函数

void show();
void autoAnimate();
void clean();
void animateControl();
int main()
{
	initgraph(width, height);
	initData();  //数据赋值
	//这个函数用于开始批量绘图。执行后，任何绘图操作都将暂时不输出到屏幕上，直到执行
	//FlushBatchDraw 或 EndBatchDraw 才将之前的绘图输出。
	BeginBatchDraw();
	while (1)
	{
		clean();  //清除小球痕迹和挡板痕迹
		fillcircle(ball_x, ball_y, radius);
		fillrectangle(bar_left, bar_top, bar_right, bar_bottom);
		autoAnimate();//清除痕迹弹力小球
        bar_left = bar_x - 100;
		bar_right = bar_x + 100;
		animateControl();//键盘输入
		autoAnimate();//弹力小球碰壁弹射
		if (ball_y + radius >= height)
		{
			
		    setbkcolor(WHITE);
			cleardevice();
			printf("很遗憾，游戏结束！");
            break;
		}

		show();       //显示挡板和小球
		Sleep(6);
		FlushBatchDraw();//这个函数用于结束批量绘制，
		                 //并执行未完成的绘制任务。

	}
	EndBatchDraw();
	_getch();
	closegraph();//这个函数用于关闭图形环境。
    return 0;
}
void initData()
{
	ball_x = width / 2;
	ball_y = height / 2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 20;
	bar_left = 220;
	bar_right = 420;
	bar_top = 450;
	bar_bottom = 480;
}
void show()
{
	//画小球
	setfillcolor(YELLOW);
	fillcircle(ball_x, ball_y, radius);
	//画挡板
	setfillcolor(RED);
	fillrectangle(bar_left, bar_top, bar_right, bar_bottom);
}
void animateControl()
{
	char inputC;
	if (_kbhit())
	{
		inputC = _getch();
		switch (inputC)
		{
		case 'A':
			if(bar_x>=0&&bar_x<=width)
			bar_x -= 50;
			else
		    bar_x=320;
			break;
		case 'D':
			if(bar_x>=0&&bar_x<=width)
			bar_x += 50;
			else
		    bar_x = 320;
			break;
		default:
			break;
		}
	}
}
void clean()
{
	setcolor(BLACK);
	setfillcolor(BLACK);
}
void autoAnimate()
{
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	if (ball_x <= radius || ball_x >= width)
	{
		ball_vx = -ball_vx;
	}
	if (ball_y <= radius || ball_y >= height)
	{
		ball_vy=-ball_vy;
	}
	//小球与挡板相撞反弹
	if (ball_x >= bar_left&&ball_x <= bar_right&&ball_y + 30 + radius >= height)
	{
		ball_vy = -ball_vy;
		ball_vx = -ball_vx; 
	}
}
