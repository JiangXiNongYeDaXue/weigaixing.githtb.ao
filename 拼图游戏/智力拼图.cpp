#include<graphics.h>
#include<conio.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define Width 900
#define Height 480
IMAGE sj[10];
IMAGE dot;
int n;
SYSTEMTIME starttime;
IMAGE all;//定义原图
IMAGE images[13];//定义各个碎图
IMAGE img;
IMAGE img1;
void time();//时间函数
void show();//布局函数
void clear();//清除函数
void loadAllImage();//读取所有图片函数
void putImage();//输出图片函数 
void mousec();
void main() {
	initgraph(Width,Height);
	GetLocalTime(&starttime);
	 clear();
	 loadAllImage();
	 putImage();
	 show();
	 while(1)
	 {
		 time();
		mousec();
	}
	 _getch();
	closegraph();
	}
//定义
void clear()
{
	setbkcolor(WHITE);
	cleardevice();

}
void loadAllImage(){
	//加载原图的各个小部分
	loadimage(&images[0],"./images2/a1.jpg");
	loadimage(&images[1],"./images2/a2.jpg");
	loadimage(&images[2],"./images2/a3.jpg");
	loadimage(&images[3],"./images2/b1.jpg");
	loadimage(&images[4],"./images2/b2.jpg");
	loadimage(&images[5],"./images2/b3.jpg");
	loadimage(&images[6],"./images2/c1.jpg");
	loadimage(&images[7],"./images2/c2.jpg");
	loadimage(&images[8],"./images2/c3.jpg");
	loadimage(&images[9],"./images2/d1.jpg");
	loadimage(&images[10],"./images2/d2.jpg");
	loadimage(&images[11],"./images2/d3.jpg");
    loadimage(&all,"./images2/all.jpg");
	//时间
	loadimage(&sj[0],_T("./images2/t0.bmp"));
	loadimage(&sj[1],_T("./images2/t1.bmp"));
	loadimage(&sj[2],_T("./images2/t2.bmp"));
	loadimage(&sj[3],_T("./images2/t3.bmp"));
	loadimage(&sj[4],_T("./images2/t4.bmp"));
	loadimage(&sj[5],_T("./images2/t5.bmp"));
	loadimage(&sj[6],_T("./images2/t6.bmp"));
	loadimage(&sj[7],_T("./images2/t7.bmp"));
	loadimage(&sj[8],_T("./images2/t8.bmp"));
	loadimage(&sj[9],_T("./images2/t9.bmp"));
	loadimage(&dot, _T("./images2/dot.bmp"));
}
void show()
{
	setlinecolor(RED);
	for(int i=0;i<8;i++)
	{
		for(int j=0;j<4;j++)
		{
			line(i*100,j*100,i*100,(j+1)*100);
		}
	}
  for(int a=0;a<7;a++)
	{
		for(int b=0;b<8;b++)
		{
			line(a*100,b*100,(a+1)*100,b*100);
		}
	}
  clearrectangle(300,0,398,400);
  settextcolor(BLACK);
  settextstyle(25,15,_T("楷体"));//设置字体
  outtextxy(125,440,_T("待拼图片"));//
  outtextxy(525,440,_T("拼图区"));

}
void putImage()
{
	int c=0;
	putimage(725,0,&all);
	putimage(800,410,&dot);
	putimage(800,430,&dot);
	for(int i=0;i<3;i++)
	{
		for(int j=0;j<4;j++)
		{
			putimage(i*100,j*100,&images[11-c]);
			c++;
		}
	}
}
void mousec(){
    getimage(&img1,300,0,100,100);
	MOUSEMSG m;
		if(MouseHit()){
			m=GetMouseMsg();
			if(m.mkLButton){//点击鼠标右键
				if(m.x<300){//选取图片
					for(int i=0;i<3;i++){
						for(int j=0;j<4;j++){
						    if(m.x>(i*100)&&m.x<(i+1)*100&&
						       m.y>(j*100)&&
							   m.y<(100*(j+1)))
							{
							    getimage(&img,i*100,j*100,100,100);
                               //putimage(i*100,j*100,&img1);
							}
						}
					}
				}
				if(m.x>400){//填充图片
					for(int i=4;i<7;i++){
						for(int j=0;j<4;j++){
			                if(m.x>(i*100)&&m.x<(i+1)*100&&
						       m.y>(j*100)&&
							   m.y<((j+1)*100))
							{
				                putimage(i*100,j*100,&img);
							}
						}
					}
				}
		   }
		}
}
void time()
{
   SYSTEMTIME currentTime; //当前时间
	GetLocalTime(&currentTime);
	int t1, t2;
	int i, j;
	int a, b;
	int sum;
	SYSTEMTIME ti;
	sum = 0;
	GetLocalTime(&ti);		// ???
	sum += ti.wHour - starttime.wHour;
	sum *= 60;
	sum += ti.wMinute - starttime.wMinute;
	sum *= 60;
	sum += ti.wSecond - starttime.wSecond;
	i = 0;
	j = 0;
	t1 = sum;
	t2 = sum / 60;
	while (i != 2)
	{
		if (i == 0)
		{
			t1 = sum % 60;
		}
		a = t1 % 10;
		putimage(845 - 32 * i, 400, &sj[a]);
		t1 = t1 / 10;
		i++;
	}
	while (j != 2)
	{
		b = t2 % 10;
		putimage(765 - 32 * j, 400, &sj[b]);
		t2 = t2 / 10;
		j++;
	}
}