#include<graphics.h>
#include<conio.h>
#define Width 640
#define Height 480
#define PI 3.14
#include<math.h>
void main()
{
	initgraph(Width,Height);
	int x,y,r=200;
	setorigin(Width/2,Height/2);
	int c;
	for(double i=0;i<PI*2;i+=0.0001){
		x=r*cos(i);
		y=r*sin(i);
		c=i*255/(2*PI);
		setlinecolor(RGB(c,0,0));
		line(0,0,x,y);
	}
	_getch();
	closegraph();
}
