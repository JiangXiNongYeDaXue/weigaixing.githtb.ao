//#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <graphics.h>
#include <conio.h>
#define Width 680
#define High 480
#define Brick_num 20 //游戏砖块的个数

//定义全局变量
int ball_x,ball_y; //小球的坐标
int ball_vx,ball_vy; //小球的速度
int radius; //小球的半径
int bar_x,bar_y; //挡板的中心位置
int bar_height,bar_width; //挡板的高度和宽度
int bar_left,bar_right,bar_top,bar_bottom; //挡板的上下左右位置坐标
int d[10][3];
IMAGE tp;
//砖块
int isBrickExisted[Brick_num]; //每个砖块是否存在,1为存在,0为没有了
int brick_height,brick_width; // 砖块的高度和宽度
int n=0,score=0;
int c[10][3] = { {255,182,193},{218,112,214},{106,90,205},{135,206,235},{255,255,0} ,{255,69,0},{220,220,220},{128,128,128},{105,105,105},{250,128,114} };

void suiji();//定义随机颜色

//数据初始化
void initData();

//创建黑色圆圈,防止绿色圆移动的时候带着尾巴.黑色圆就是用来打底(清除痕迹)
void clean();

//画其他颜色的圆
void show();

//与用户输入无关的更新
void updateWithoutInput();

//与用户更新相关的更新
void updateWithInput();

//游戏结束的函数
void gameOver();


int _tmain(int argc, _TCHAR* argv[])
{
    suiji();
	initData();
	while(1){
		clean();
		updateWithoutInput();
		updateWithInput();
		show();
	}
	gameOver();
	return 0;
}

void initData(){
	//初始化圆球的信息
	ball_x = Width/2;
	ball_y = High/2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 10;

	//初始化挡板的信息
	//宽高 和中心坐标
	bar_width = Width/5;
	bar_height = High/20;
	bar_x = Width/2;
	bar_y = High - bar_height/2;
	//上下左右坐标
	bar_left = bar_x - bar_width/2;
	bar_right = bar_x + bar_width/2;
	bar_top = bar_y - bar_height/2;
	bar_bottom = bar_y + bar_height/2;
	//初始化砖块信息
	brick_width = Width/10;//长度
	brick_height = High/15;//高度
	int i;
	//设置默认所有砖块都存在
	for(i=0;i<Brick_num;i++){
		isBrickExisted[i] = 1;
	}
}

void suiji(){
	int m,a;
	srand((unsigned)time(NULL));
	for(m=0;m<20;m++)
	{
	  a=rand()%10+1;
	  d[m][3]=c[m][3];//颜色数组赋值
	}
	initgraph(Width,High);
	BeginBatchDraw();//开始批量绘图
}
//消除
void clean() {
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x,ball_y,radius);//小球
	bar(bar_left,bar_top,bar_right,bar_bottom);//挡板
	int i,j,brick_left,brick_right,brick_top,brick_bottom;
	for(i = 0;i<Brick_num;i++){
		if(i<10){
		brick_left = i*brick_width;
		brick_right = brick_left + brick_width;
		brick_top = 0;
		brick_bottom = brick_height;
		//砖块存在,绘制砖块
		if(!isBrickExisted[i]){  //0
			fillrectangle(brick_left,brick_top,brick_right,brick_bottom);}
		}
		else
		{
			j=i-10;
			brick_left = j*brick_width;
		    brick_right = brick_left + brick_width;
		    brick_top = brick_height;
		    brick_bottom = 2*brick_height;
		    //砖块存在,绘制砖块
		     if(!isBrickExisted[i]){  //0
			fillrectangle(brick_left,brick_top,brick_right,brick_bottom);}

		}
	}
}

//显示
void show(){
	setcolor(YELLOW);
	setfillcolor(CYAN);
	fillcircle(ball_x,ball_y,radius);
	setcolor(YELLOW);
	setfillcolor(YELLOW);
	bar(bar_left,bar_top,bar_right,bar_bottom);
	int i,j,brick_left,brick_right,brick_top,brick_bottom;
	for(i = 0;i<20;i++){
		if(i<10){
		brick_left = i*brick_width;
		brick_right = brick_left + brick_width;
		brick_top = 0;
		brick_bottom = brick_height;
		//砖块存在,绘制砖块
		if(isBrickExisted[i]){
			setcolor(WHITE);
			setfillcolor(RGB(d[i][0],d[i][1],d[i][2]));
			fillrectangle(brick_left,brick_top,brick_right,brick_bottom);}
		}
		else
		{
		j=i-10;
		brick_left = j*brick_width;
		brick_right = brick_left + brick_width;
		brick_top = brick_height;
		brick_bottom = 2*brick_height;
		//砖块存在,绘制砖块
		if(isBrickExisted[i]){
			setcolor(WHITE);
			setfillcolor(RGB(d[10-j][0],d[10-j][1],d[10-j][2]));
			fillrectangle(brick_left,brick_top,brick_right,brick_bottom);}
		}
	}
	FlushBatchDraw();
	Sleep(5);
}

void updateWithoutInput(){

	//挡板和小球触碰,小球反弹
	if(((ball_y+radius>=bar_top)&&(ball_y+radius<bar_bottom-bar_height/3))
		|| ((ball_y-radius<=bar_bottom)&&(ball_y-radius>bar_top-bar_height/3))
		){
			if((ball_x>=bar_left)&& (ball_x<=bar_right)){
				ball_vy = -ball_vy;
			}
	}
	//判断小球是否和底部接触,如果是则游戏结束,小球停止移动
	if((ball_y+radius*2)==High){
		//// 初始化绘图窗口
	    initgraph(640, 480);
		score=n*5;
       outtextxy(Width/2-10,High/2-10,"游戏结束！");
		outtextxy(Width/2-10,High/2+20,"得分为：");		
		char s[2];
        sprintf(s,"%d",score);
		outtextxy(Width/2+50,High/2+20,s);
		_getch();
		closegraph();
	}
	if(n==20){
		//// 初始化绘图窗口
	    initgraph(640, 480);
		score=n*5;
       outtextxy(Width/2-10,High/2-10,"游戏结束！");
		outtextxy(Width/2-10,High/2+20,"得分为：");		
		char s[2];
        sprintf(s,"%d",score);
		outtextxy(Width/2+50,High/2+20,s);
		_getch();
		closegraph();
	}
		
	//更新小圆的坐标
					
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	if((ball_x<=radius)||(ball_x>=Width-radius)){
		ball_vx = -ball_vx;
	}
	if((ball_y<=radius)||(ball_y>=High-radius)){
		ball_vy = -ball_vy;
	}

	//判断小球是否和某个砖块碰撞
	int i,j,brick_left,brick_right,brick_bottom;
	for(i=0;i<20;i++){
		if(i<10){                //第一层
            if(isBrickExisted[i]){
			brick_left = i*brick_width;
			brick_right = brick_left + brick_width;
			brick_bottom =  brick_height;
			if((ball_y == brick_bottom + radius) && (ball_x>=brick_left) && (ball_x<= brick_right)){
				isBrickExisted[i] = 0;
				n=n+1;
				ball_vy = -ball_vy;}
			}
			}
			else
			{
				if(isBrickExisted[i]){
				j=i-10;         //第二层
				brick_left = j*brick_width;
			    brick_right = brick_left + brick_width;
			    brick_bottom =  2*brick_height;
		     	if((ball_y == brick_bottom + radius) && (ball_x>=brick_left) && (ball_x<= brick_right)){
				isBrickExisted[i] = 0;
				n=n+1;
				ball_vy = -ball_vy;}
				}
			}
		}
	}



//用户输入处理
void updateWithInput(){
	char input;
	if(_kbhit()){
		input = _getch();
		if(input == 'A' && bar_left>0){
			bar_x = bar_x - 45;
			bar_left = bar_x - bar_width/2;
			bar_right = bar_x + bar_width/2;
		}
		//右移
		if(input == 'D' && bar_right<Width){
			bar_x = bar_x + 45;
			bar_left = bar_x -bar_width/2;
			bar_right = bar_x + bar_width/2;
		}
	}
}

void gameOver(){
	EndBatchDraw();
	closegraph();
}